terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "2.60.0"
    }
  }
}

provider "azurerm" {
  features {}
}

variable "resource_group_name" {
  type    = string
  default = "lab02-rg"
}

variable "location" {
  type    = string
  default = "Canada Central"
}

variable "virtual_net_name" {
  type    = string
  default = "virtualNetwork1"
}

variable "virtual_net_address_space" {
  type    = list(string)
  default = ["10.0.0.0/16"]
}

variable "subnet01_name" {
  type    = string
  default = "lab02-subnet1"
}

variable "subnet02_name" {
  type    = string
  default = "lab02-subnet2"
}

variable "subnet01_address_space" {
  type    = list(string)
  default = ["10.0.1.0/24"]
}

variable "subnet02_address_space" {
  type    = list(string)
  default = ["10.0.2.0/24"]
}

variable "network_security_group_01_name" {
  type    = string
  default = "lab02-nsg01"
}

variable "network_security_group_02_name" {
  type    = string
  default = "lab02-nsg02"
}
